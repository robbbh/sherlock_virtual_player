# usr/bin/python
import time, datetime
import sys
import numpy as np
import cv2
import os
from os import listdir
from os.path import isfile, join, dirname, realpath
import json
import requests
import traceback
import threading
import re
import logging

class Player_Input_Handle:

    def __init__(self):
        """ Gets player messages from the cloud instance. Use the questions 
            as input to the virtual player """
        self.communication = Communication_Module()
        
    def launch(self):
        """ Launches the ai_player and scans for new input. """

        self.init_logging()
        question_list = []
        launch_message = "AI Player Launched!"
        print launch_message
        logging.info(launch_message)

        while True:
            found_ask_cards = self.get_requests_from_players()
            question_scope = self.communication.get_active_questions()

            for question in found_ask_cards:
                question_player_tuple = (question[0], question[2])
                if question_player_tuple not in question_list:
                    t = threading.Thread(target=self.check_message_type, args=(question, question_scope,))
                    t.start() 
                    question_list.append(question_player_tuple)

            time.sleep(10)
            polling_message =  "Checking for new questions..."
            logging.info(polling_message)
            print polling_message

    def init_logging(self):
        dt = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S')
        filename = 'sherBot_' + dt + '.log'
        directory_filename = 'log/' + filename
        # change to logging.DEBUG for debug to output to logs
        debug_level = logging.INFO

        try:
            with open(directory_filename, 'a'):
                os.utime(directory_filename, None)
            logging.basicConfig(filename=directory_filename, level=debug_level)

        except:
            os.makedirs('log')
            with open(directory_filename, 'a'):
                os.utime(directory_filename, None)
            logging.basicConfig(filename=directory_filename, level=debug_level)



    def get_requests_from_players(self):
        """ Gets all instances from the bot request cloud CEServer and 
            checks for ask cards. questions, players and agent names are then
            extracted. """

        # extracted data = question, agent name, player name
        question_and_owner = []
        ask_card_id = []
        all_cards = self.communication.get_requests()

        for card_type in all_cards:
            if card_type["conceptName"] == "gist card":
                ask_card_id.append(card_type["id"])

        for id in ask_card_id:
            card = self.communication.get_requests_with_id(id)

            for question in card["values"]:
                if question["label"] == "content":
                    extracted_question = question["targetName"]
            
            for agent in card["relationships"]:
                if agent["label"] == "is from":
                    agent_name = agent["targetName"]

            player_name = agent_name.split()[0]

            question_and_owner.append((extracted_question, agent_name, player_name))

        if len(question_and_owner) == 0:
            no_question_message = "No questions to act on at this time."
            logging.info(no_question_message) 
            print no_question_message
        
        return question_and_owner


    def check_message_type(self, questions, question_scope):
        extracted_question = questions[0]
        agent_name = questions[1]
        player_name = questions[2]

        interpret_text = Linguistic_Module(agent_name, player_name, question_scope)
        interpret_text.check_if_question_exists(str(extracted_question))


class Visual_Module:

    def __init__(self):
        """ Apply's the SIFT algoriothm for object classification based off
             training images. """

        self.current_directory = dirname(realpath(__file__))
        self.train_scene_direc = "scenes/"
        self.train_obj_direc = "objects/"
        self.scenes_direc = join(self.current_directory, self.train_scene_direc)
        self.scenes = [f for f in listdir(self.scenes_direc) if isfile(join(self.scenes_direc, f))]


    def find_scenes_with_objects(self, subject_data, object_data):
        """ Searches for Objects within scenes. """
        positive_scenes = []
        object_to_find = subject_data[0]
        maximum_distance = subject_data[1]
        minimum_matches = subject_data[2]
        input_image_name = subject_data[3]


        for scene in self.scenes:
            positive_scenes.append(self.test_image(self.train_scene_direc + scene, self.train_obj_direc + object_to_find, minimum_matches, maximum_distance, input_image_name))

        scenes_with_object = [i[0] for i in positive_scenes if i != None]

        if len(scenes_with_object) is 0:
            no_scenes_error = "No Scenes found with object."
            logging.warning(no_scenes_error)
            print no_scenes_error

        else:
            subject = self.find_objects_within_refined_scenes(scenes_with_object, object_data)
            return subject
        


    def find_objects_within_refined_scenes(self, scenes_with_object, object_data):
        """ Searches for objects within refined scenes. """
        answer_list = []

        for details in object_data:
            input_image = details[0]
            maximum_distance = details[1]
            minimum_matches = details[2]  
            input_image_name = details[3]
            logging.debug("Refined scenes input = input_image: {0}, max_dist: {1}, min_match: {2}, image_name: {3}".format(input_image, maximum_distance, minimum_matches, input_image_name))

            for scene in scenes_with_object:
                answer_list.append(self.test_image(scene, self.train_obj_direc + input_image,  minimum_matches, maximum_distance, input_image_name))

        answer = "\n".join(set([i[1] for i in answer_list if i != None]))

        if len(answer) is 0:
            no_scenes_error = "No refined scenes found with object."
            logging.warning(no_scenes_error)
            print no_scenes_error
        else:
            return answer


    def test_image(self, test, train, min_match, max_dist, input_image_name):
        """ Performs image classification with the SIFT descriptor """
        # opens query and train iamge in grayscale
        query_image = cv2.imread(test,0)
        train_image = cv2.imread(train,0) 

        # Initiate SIFT detector
        sift = cv2.xfeatures2d.SIFT_create()

        # find the keypoints and descriptors with SIFT
        try:
            kp1, des1 = sift.detectAndCompute(query_image,None)
            kp2, des2 = sift.detectAndCompute(train_image,None)
        except:
            tb = traceback.format_exc()
            logging.error(tb)
            print tb
            os._exit(0)

        FLANN_INDEX_KDTREE = 0
        index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
        search_params = dict(checks = 50)

        flann = cv2.FlannBasedMatcher(index_params, search_params)

        matches = flann.knnMatch(des1,des2,k=2)

        return self.get_best_match(matches, test, train, max_dist, min_match, input_image_name)       


    def get_best_match(self, matches, test, train, max_dist, min_match, input_image_name):
        """ Works out the best match based on training data. """

        # store all the good matches as per Lowe's ratio test.
        good = []
        for m,n in matches:
            if m.distance < 0.7*n.distance:
                good.append(m) 

        logging.debug("lvl 1 = query_image: {0} \t train_image: {1} \t query_image_matches: {2} v train_image_matches: {3}".format(test, train, len(good), min_match))

        if len(good) >= int(min_match):
            match_array = [matches.distance for matches in good]
            # sorts matches by size
            matches = sorted(match_array, key = lambda x:x)
            # gets average of 20 best matches
            distance = sum(matches[:20])/20
            logging.debug("lvl 2 = query_image: {0} \t train_image: {1} \t query_image_distance: {2} v train_image_distance: {3}".format(test, train, distance, max_dist))


            if distance <= int(max_dist):
                scene_path = test
                image = input_image_name
                logging.debug("lvl 3 = query_image: {0} \tvs\t train_image: {1}\t\t (matches, distance) = ({2}, {3})".format(test, train, len(good), distance))
                return scene_path, input_image_name


class Communication_Module:

    def __init__(self):
        """ Performs system communication through the Requests API"""
        self.port = 8004
        self.request_port = 1337


    def post_model(self):
        try:
            with open('sherlock.ce', 'r') as sherlock_model:
                model = sherlock_model.read()
            requests.post('http://localhost:' + str(self.port) + '/sentences', data = model)
        except requests.exceptions.RequestException as e:
            error_message = "Error posting model to instance. Please check your connection with the localhost CEServer.\n"
            print error_message
            print e
            logging.error(error_message)
            logging.error(e)
            sys.exit(0)        


    def get_requests(self):
        try:
            response = requests.get('http://explorer.cenode.io:' + str(self.request_port) + '/instances').json()
            return response            
        except requests.exceptions.RequestException as e:
            error_message = "Unable to get requests. Retrying connection.\n"
            print error_message
            print e
            logging.error(error_message)
            logging.error(e)

            while True:
                retry_error = "Retrying connection..."
                print retry_error
                logging.error(retry_error)
                try:
                    time.sleep(5)
                    response = requests.get('http://explorer.cenode.io:' + str(self.request_port) + '/instances').json()
                    break
                except:
                    continue
            return response
                


    def get_requests_with_id(self, id):
        try:
            response = requests.get('http://explorer.cenode.io:' + str(self.request_port) + '/instance?id=' + str(id)).json()
            return response            
        except requests.exceptions.RequestException as e:
            error_message = "Unable to get requests with ID. Please check your connection with the cloud CEServer instance.\n"
            print error_message
            print e
            logging.error(error_message)
            logging.error(e)
            os._exit(0)


    def post_to_shared_kb(self, tellcard):
        try:
            requests.post("http://explorer.cenode.io:" + self.myCroft_port + "/sentences", data = tellcard)
        except requests.exceptions.RequestException as e:
            error_message = "Posting to shared knowledge base failed. Please check your connection with the external instance at http://explorer.cenode.io.\n"
            print error_message
            print e
            logging.error(error_message)
            logging.error(e)


    def post_to_local_kb(self, tellcard):
        try:
            requests.post('http://localhost:' + str(self.port) + "/sentences", data = tellcard)
        except requests.exceptions.RequestException as e:
            error_message = "Posting to local knowledge base failed. Please check your connection with the local instance at localhost.\n" 
            print error_message
            print e
            logging.error(error_message)
            logging.error(e)


    def get_instances(self):
        try:
            response = requests.get('http://localhost:' + str(self.port) + '/instances').json()
            return response            
        except requests.exceptions.RequestException as e:
            error_message = "Unable to get instances. Please check your connection with the localhost CEServer.\n"
            print error_message
            print e
            logging.error(error_message)
            logging.error(e)
            os._exit(0)

    def get_concepts(self):
        try:
            response = requests.get('http://localhost:' + str(self.port) + '/concepts').json()
            return response            
        except requests.exceptions.RequestException as e:
            error_message = "Unable to get concepts. Please check your connection with the localhost CEServer.\n"
            print error_message
            print e
            logging.error(error_message)
            logging.error(e)
            os._exit(0)


    def get_model_concept_with_id(self, id):
        try:
            response = requests.get("http://localhost:" + str(self.port) + "/concept" + "?id=" + str(id)).json()
            return response            
        except requests.exceptions.RequestException as e:
            error_message = "Unable to get concept with ID. Please check the concept ID exists and your connection with the localhost CENode exists.\n"
            print error_message
            print e
            logging.error(error_message)
            logging.error(e)
            os._exit(0)


    def get_model_instance_with_id(self, id):
        try:
            response = requests.get("http://localhost:" + str(self.port) + "/instance" + "?id=" + str(id)).json()
            return response            
        except requests.exceptions.RequestException as e:
            error_message = "Unable to get instance ID. Please check the instance ID exists and your connection with the localhost CENode exists.\n"
            print error_message
            print e
            logging.error(error_message)
            logging.error(e)
            os._exit(0)


    def get_active_questions(self):
        """ Gets active questions based on the active_question.txt text file.
	    This will limit the scope of the bots ability to 
	    perceive its environment """
        try:
            with open('active_questions.txt', 'r') as active_questions:
                questions = active_questions.read().split(",")
                questions = [x.strip("\n") for x in questions]
        except IOError:
            error_message = "Cannot read active questions. Question scoping disabled."
            print error_message
            logging.error(error_message)
            questions = []
        return questions



class Linguistic_Module:

    def __init__(self, agent_name, player_name, scope_questions):

        self.sherlock_model = Communication_Module()
        self.instance_data = self.sherlock_model.get_instances()
        self.concept_data = self.sherlock_model.get_concepts()
        self.sift_parameters = self.get_sift_parameters()
        self.player_name = player_name
        self.agent_name = agent_name
        self.scope_questions = scope_questions

        # 'What colour hat is Elephant wearing?' type question
        self.question_group_1 = ["q4", "q7", "q24", "q41"]
        # "Where is the apple?"" type question
        self.question_group_2 = ["q6", "q8", "q25", "q47", "q48", "q53"]
        # 'Which character is in the emerald room?' type question
        self.question_group_3 = ["q9", "q13", "q37", "q45", "q28"]


    def get_sift_parameters(self):
        """ Gets SIFT parameters relating to game objects"""
        concepts = self.concept_data
        for concept_name in self.concept_data:
            if concept_name["name"] == "sift parameter set":
                concept_id = concept_name["id"]
                self.sift_parameters = self.sherlock_model.get_model_concept_with_id(concept_id)
        return self.sift_parameters
	    

    def check_if_question_exists(self, input_string):
        """ Take question and check if it exists in the model. If it exists, extract 
             useful data. """
        input_string = input_string.lower()
        question_data = self.get_model_questions()
        found = False
        agentName = "sherBotAgent"
        botName = "sherBot"
        cleansed_input_string = self.cleanse_string(input_string)
        cleansed_scope_questions = [self.cleanse_string(str(q).lower()) for q in self.scope_questions]

        for question in question_data:
            cleansed_question = self.cleanse_string(str(question["values"][0]["targetName"]).lower()) 
            if cleansed_question == cleansed_input_string:
                if cleansed_input_string in cleansed_scope_questions:
                    subjectType = question["relationships"][0]["targetConceptName"]
                    subjectName = question["relationships"][0]["targetName"]
                    relationship = question["values"][1]["targetName"]
                    questionID = question["name"]
                    found = True

        if found:
            success_print = "Player {0}: '{1}' is a valid question, forwarding for processing.".format(self.player_name, input_string)
            print success_print
            logging.info(success_print)
            self.process_question_data(subjectType, subjectName, relationship, questionID)
        else:
            error_response_output = "Invalid or Out of Scope question - \\'{1}\\'".format(self.player_name, input_string)
            error_response_print = "Player {0}: Invalid or Out of Scope question - '{1}'".format(self.player_name, input_string)
            print error_response_print
            logging.error(error_response_print)
            gist_response = """there is a gist card named 'msg_{{uid}}' that has '{0}' as content and is to the agent '{1}' and is from the agent '{2}' and is to the agent '{3}' and is to the individual '{4}' and has the timestamp '{{now}}' as timestamp""".format(error_response_output, agentName, botName, self.agent_name, self.player_name)
            logging.debug(gist_response)
            self.sherlock_model.post_to_local_kb(gist_response)
            return

 
    def cleanse_string(self, sentence):
        """ Removes stopwords and symbols 
            from input and returns the a cleansed sentence. """

        stop_words = [u'i', u'me', u'my', u'myself', u'we', u'our', u'ours', 
        u'ourselves', u'you', u'your', u'yours', u'yourself', 
        u'yourselves', u'he', u'him', u'his', u'himself', 
        u'she', u'her', u'hers', u'herself', u'it', u'its', 
        u'itself', u'they', u'them', u'their', u'theirs', 
        u'themselves', u'who', u'whom', 
        u'this', u'that', u'these', u'those', u'am', u'is', 
        u'are', u'was', u'were', u'be', u'been', u'being', 
        u'have', u'has', u'had', u'having', u'do', u'does', 
        u'did', u'doing', u'a', u'an', u'the', u'and', u'but', 
        u'if', u'or', u'because', u'as', u'until', u'while', 
        u'of', u'at', u'by', u'for', u'with', u'about', u'against', 
        u'between', u'into', u'through', u'during', u'before', u'after', 
        u'above', u'below', u'to', u'from', u'up', u'down', u'in', u'out', 
        u'on', u'off', u'over', u'under', u'again', u'further', u'then', u'once', 
        u'here', u'there', u'when', u'why', u'how', u'all', u'any', 
        u'both', u'each', u'few', u'more', u'most', u'other', u'some', u'such', 
        u'no', u'nor', u'not', u'only', u'own', u'same', u'so', u'than', u'too', 
        u'very', u's', u't', u'can', u'will', u'just', u'don', u'should', u'now', 
        u'd', u'll', u'm', u'o', u're', u've', u'y', u'ain', u'aren', u'couldn', 
        u'didn', u'doesn', u'hadn', u'hasn', u'haven', u'isn', u'ma', u'mightn', 
        u'mustn', u'needn', u'shan', u'shouldn', u'wasn', u'weren', u'won', u'wouldn']

        words = filter(None, re.split("[, \-!?:]+", sentence))

        new_string = []

        for w in words:
            if w not in stop_words:
                new_string.append(w)

        string = " ".join(new_string)

        return str(string).strip()
 

    def get_model_questions(self):
        """ gets the questions associated with the sherlock game. """
        question_data = []
        for instance in self.instance_data:
            if instance["conceptName"] == "question":
                question_data.append(self.sherlock_model.get_model_instance_with_id(str(instance["id"])))

        return question_data
        

    def process_question_data(self, subjectType, subjectName, relationship, questionID):
        """ Searches the model for other useful data relating to the question.
            different question formats have a different method of retreiving data.
            These questions ids have been placed in groups in the constructor. """

        objectType = ""

        try:

            # 'What colour hat is Elephant wearing?' type question
            if questionID in self.question_group_1:
                objectType = relationship
                for concepts in self.concept_data:
                    if concepts["name"] == relationship:
                        subjectTypeID = concepts["id"]
                        subjectTypeConcept = self.sherlock_model.get_model_concept_with_id(subjectTypeID)
                        relationship = subjectTypeConcept["relationships"][0]["label"]

                
            # "Where is the apple?"" type question
            elif questionID in self.question_group_2:
                for concepts in self.concept_data:
                    if concepts["name"] == subjectType:
                        concept_id = concepts["id"]

                subjectTypeConcept = self.sherlock_model.get_model_concept_with_id(concept_id)        

                for concepts in subjectTypeConcept["parents"]:
                    if concepts["name"] == "locatable thing":
                        concept_id = concepts["id"]

                subjectTypeConcept = self.sherlock_model.get_model_concept_with_id(concept_id)

                for objectTypeValue in subjectTypeConcept["relationships"]:
                    locatableID = objectTypeValue["targetId"]

                objectTypeConcept = self.sherlock_model.get_model_concept_with_id(locatableID)

                for objectTypeValue in objectTypeConcept["children"]:
                    objectType = objectTypeValue["name"]     


            # 'Which character is in the emerald room?' type question
            elif questionID in self.question_group_3:
                for concepts in self.concept_data:
                    if concepts["name"] == subjectType:
                        subjectTypeID = concepts["id"]

                subjectTypeConcept = self.sherlock_model.get_model_concept_with_id(subjectTypeID)

                for conceptValues in subjectTypeConcept["values"]:
                    if conceptValues["label"] == relationship:
                        objectType = conceptValues["targetName"]
                        objectTypeID = conceptValues["targetId"]
                        break

                for conceptValues in subjectTypeConcept["relationships"]:         
                    if conceptValues["label"] == relationship: 
                        objectType = conceptValues["targetName"]
                        objectTypeID = conceptValues["targetId"]
                        break

                objectTypeConcept = self.sherlock_model.get_model_concept_with_id(objectTypeID)

                for objectTypeValue in objectTypeConcept["parents"]:
                    if objectTypeValue["name"] == "locatable thing":
                        locatableID = objectTypeValue["id"]

                objectTypeConcept = self.sherlock_model.get_model_concept_with_id(locatableID)

                for objectTypeValue in objectTypeConcept["relationships"]:
                    relationship = objectTypeValue["label"]
                    locatableID = objectTypeValue["targetId"]

                objectTypeConcept = self.sherlock_model.get_model_concept_with_id(locatableID)

                for objectTypeValue in objectTypeConcept["children"]:
                    subjectType = objectTypeValue["name"]                    


            # All other questions
            else:
                for concepts in self.concept_data:
                    if concepts["name"] == subjectType:
                        subjectTypeID = concepts["id"]

                subjectTypeConcept = self.sherlock_model.get_model_concept_with_id(subjectTypeID)

                for conceptValues in subjectTypeConcept["relationships"]:
                    if conceptValues["label"] == relationship:
                        objectType = conceptValues["targetName"]
                        break

            message_to_screen = "Player {2}: We're looking for '{0}' associated with '{1}'".format(objectType, subjectName, self.player_name)
            print message_to_screen
            logging.info(message_to_screen)
        except:
            error_message = "Could not gather all data from the model"
            print error_message
            tb = traceback.format_exc()
            print tb
            logging.error(error_message)
            logging.error(tb)
            return


        self.search_environment(objectType, subjectName, subjectType, relationship, questionID)


    def search_environment(self, objectType, subjectName, subjectType, relationship, questionID):
        """ Searches the environment of posters for objects"""

        subject_data = self.get_subjectName_image_path_and_params(subjectName)
        object_data = self.get_object_image_paths_and_params(objectType)            

        image_recognition = Visual_Module()
        found_object = image_recognition.find_scenes_with_objects(subject_data, object_data)
  

        self.construct_output(subjectType, subjectName, relationship, objectType, found_object, questionID)


    def get_subjectName_image_path_and_params(self, subjectName):
        """ Gets the subjectName image path and get training parameters
            for the image"""

        for index_entry in self.instance_data:
            if index_entry["name"] == subjectName:
                objectID = index_entry["id"]


        instance_id_data = self.sherlock_model.get_model_instance_with_id(objectID)

        subject_image_path = str(instance_id_data["values"][0]["targetName"])
        subject_image_name = str(instance_id_data["name"])

        for sift_obj in self.sift_parameters["instances"]:
            if str(sift_obj["name"]).lower() == subjectName.lower() + " parameters":
                siftID = sift_obj["id"]

        instance_id_data = self.sherlock_model.get_model_instance_with_id(siftID)
        
        for value in instance_id_data["values"]:
            if value["label"] == "maximum distance":
                max_distance = value["targetName"]
            elif value["label"] == "minimum matches":
                min_matches = value["targetName"]

        parameters = (subject_image_path, max_distance, min_matches, subject_image_name)
        return parameters
        

    def get_object_image_paths_and_params(self, objectType):
        """ Gets the objectType image paths and get training parameters
            for each of the images. """

        objectID_list = []
        object_name_list = []
        object_image_path = []
        object_image_name = []
        max_distance = []
        min_matches = []
        parameters = []

        for index_entry in self.instance_data:
            if index_entry["conceptName"] == objectType:
                objectID_list.append(index_entry["id"])
                object_name_list.append(index_entry["name"])

        for id_value in objectID_list:
            instance_id_data = self.sherlock_model.get_model_instance_with_id(id_value)
            object_image_path.append(str(instance_id_data["values"][0]["targetName"]))
            object_image_name.append(str(instance_id_data["name"]))

        for obj_name in object_name_list:
            for sift_obj in self.sift_parameters["instances"]:
                if str(sift_obj["name"]).lower() == obj_name.lower() + " parameters":
                    siftID = sift_obj["id"]

            instance_id_data = self.sherlock_model.get_model_instance_with_id(siftID)

            for value in instance_id_data["values"]:
                if value["label"] == "maximum distance":
                    max_distance.append(value["targetName"])
                elif value["label"] == "minimum matches":
                    min_matches.append(value["targetName"])

        for x, y, z, n in zip(object_image_path, max_distance, min_matches, object_image_name):
            parameters.append((x,y,z,n))

        return parameters


    def construct_output(self, subjectType, subjectName, relationship, objectType, found_object, questionID):
        """ Prints the output to the bot and to terminal """
        if questionID in self.question_group_1 or questionID in self.question_group_3:
            message_to_output = "The {3} \\'{4}\\' {2} the {0} \\'{1}\\'".format(subjectType, subjectName, relationship, objectType, found_object)
            message_to_print = "The {3} '{4}' {2} the {0} '{1}'".format(subjectType, subjectName, relationship, objectType, found_object)
        else:
            message_to_output = "The {0} \\'{1}\\' {2} the {3} \\'{4}\\'".format(subjectType, subjectName, relationship, objectType, found_object)
            message_to_print = "The {0} '{1}' {2} the {3} '{4}'".format(subjectType, subjectName, relationship, objectType, found_object)


        message = "Player {1}: CNL, '{0}'".format(message_to_print, self.player_name)
        print message
        logging.info(message)

        agentName = "sherBotAgent"
        botName = "sherBot"
        tellcard = """there is a tell card named 'msg_{{uid}}' that has '{0}' as content and is to the agent '{1}' and is from the agent '{2}' and is to the agent '{3}' and is to the individual '{4}' and has the timestamp '{{now}}' as timestamp""".format(message_to_output, agentName, botName, self.agent_name, self.player_name)
        logging.debug(tellcard)
        self.sherlock_model.post_to_local_kb(tellcard)


if __name__ == "__main__":

    Communication_Module().post_model()
    Player_Input_Handle().launch()


