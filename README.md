# SHERLOCK virtual player

This repository contains the source code for a virtual player of the ITA [SHERLOCK](https://github.com/flyingsparx/sherlock) game. Combining computer vision, controlled natural language (CNL) and reasoning, the player can perceive, describe and communicate the SHERLOCK environment. It receives its visual input through feeds of poster images and is trained to recognise game objects.

The player utilises a local [CENode](https://github.com/flyingsparx/CENode) instance which is a pure JavaScript implementation of the ITA project's CEStore. The virtual player uploads a customised SHERLOCK CE model to a local CENode instance to understand the world around it. It then interacts with cloud CE instances to receive player inputs and relay outputs back to the players. Using the sherlock conversational interface, the virtual player can be prompted with the `@sherbot` prefix followed by a question.  

# Dependencies 
## Python
The SHERLOCK virtual player depends on several external Python packages to function correctly. 
These packages include
[OpenCV](http://opencv.org/),
[Numpy](http://www.numpy.org/),
[Requests](http://docs.python-requests.org/en/master/).

A recommended tutorial to install the Python wrapper for OpenCV can be found [here](http://www.pyimagesearch.com/2016/10/24/ubuntu-16-04-how-to-install-opencv/). Numpy and Requests can be installed using the pip installer.

## JavaScript

CENode depends on node.js being installed. Will Webberly provides a set-up guide for CENode dependencies which can be found [here](https://github.com/flyingsparx/CENode/wiki/Getting-Started-Guide#installing-node-and-npm).

# Usage

## Cloud CE Server Instances
The virtual player receives its requests from a cloud CEServer instance which are currently hosted on [explorer.cenode.io](http://explorer.cenode.io/#/). 
Two CE server instances will need to be launched with the following details:

The Mycroft agent will act as the central gossip node and relay tell/ gist cards to differnt players.

   * **Agent Name**: Mycroft
   * **Port Number**: 8888
   * **CE Models**: Core + Server
   
sherBotRequest is where requests to the virtual player are read and acted upon.

   * **Agent Name**: sherBotRequest
   * **Port Number**: 1337
   * **CE Models**: Core + Server

## Launching the local CENode instance
Before the virtual player can be launched, a local CENode instance must be running. This can be done by naviagating to the `/cenode/src` directory, and executing the `CEServer.js` file with the following arguments. If CENode has been installed using npm, then it will most likely be found in the `node_modules` directory. **_Note: The virtual player is currently listening on port 8004 and for the agent name 'sherBotAgent'. The virtual player will not work if these values are changed._**

```bash
node ./SEServer.js sherBotAgent 8004 core
``` 

 This will initialise a local CENode instance named `sherBotRequest` running on port `8004` with the `core` functionalities running. 

## Launching the virtual player
Once the CENode server instance is operational, the virtual player can be launched. Assuming all Python dependencies have been satisfied, it can be launched by executing the `ai_player.py` file with the following command.

```python 
python ai_player.py 
```

## Interacting with the virtual player
The virtual player can be prompted using a modified verison of the SHERLOCK game. A modified version of the game is currently being hosted at [rob.cenode.io](http://rob.cenode.io) The source code can be found at [https://bitbucket.org/robbbh/modified_sherlock_game](https://bitbucket.org/robbbh/modified_sherlock_game).

### Prompting the virtual player
The virtual player can be prompted within the sherlock conversational interface using the `@sherbot` prefix. A question from the instruction sheet must be asked to the virtual player.

Example questions:

* `@sherbot` Where is Elephant?"
* `@sherbot` What sport does Hippopotamus play?
* `@sherbot` Which character is in the emerald room?
* `@sherbot` What character is wearing a yellow hat?
  
All successfully answered questions will be added to the knowledge base where all players can observe.

## Limiting the Scope

It is possible to limit the scope of the virtual player by limiting the questions it can answer. The aim of this functionality is to emulate differing levels of access the virtual player has to its environment. The list of questions the virtual player can answer can be found in the `active_questions.txt` file. Each question is seperated by a comma and a new line. By default the virtual player can answer every question. To limit the scope, one must delete the questions they do not wish the virtual player to answer. 

# Licensing 

The contents of this repository are licensed under the Apache License v2. See LICENSE for further information.

# Contact Me

This Code Base is owned and maintained by Robert Harris. Please e-mail **harrisrp95@gmail.com** for any questions or suggestions.
